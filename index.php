<!DOCTYPE html>
<html lang="en">
<head>
    <title>Auto Deployment</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <style type="text/css">
        body {
            background-color: red;
            color: white;
            margin: 0;
            padding: 0;
        }
        .container{
            width: 100%;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="container">
	<?php
	echo "Hello Autodeployment<br>";
	echo "Now i can be deployed automatically<br>";
	echo "Second Try<br>";
	echo "Third Try<br>";
	echo "Fourth Try<br>";
	echo "Fifth Try<br>";
	echo "Sixth Try<br>";
	echo "Seventh Try<br>";
	echo "Eight Try<br>";
	echo "Ninth Try<br>";
	?>
</div>
</body>
</html>
